import { LitElement, html} from "lit-element";
import '../persona-ficha-listado/persona-ficha-listado'
import '../persona-form/persona-form'
import '../persona-main-dm/persona-main-dm'

class PersonaMain extends LitElement{

    static get properties(){
        return{
            people:{type: Array},
            showPersonForm: {type: Boolean}
        };
    }

    constructor(){
        console.log("Constructor persona-main");
        super();
        
        this.people = []

        this.showPersonForm = false;
    }
    
    render(){
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
            <h2 class="text-center">Personas</h2>
            <div class="row" id="peopleList">
                <div class="row row-cols-1 row-cols-sm-4">
                ${this.people.map(
                    person => html  `<persona-ficha-listado 
                                        fname="${person.name}"
                                        yearsInCompany="${person.yearsInCompany}"
                                        profile="${person.profile}"
                                        .photo="${person.photo}"
                                        @delete-person="${this.deletePerson}"
                                        @info-person="${this.infoPerson}"
                                    </persona-ficha-listado>`
                )}
                </div>
            </div>
            <div class="row">
                <persona-form 
                    @persona-form-close="${this.personFormClose}" 
                    @persona-form-store="${this.personFormStore}" 
                    class="d-none border rounded border-primary" id="personForm">
                </persona-form>            
            </div>
            <persona-main-dm 
                @persona-data-updated="${this.peopleDataUpdated}"
            </persona-main-dm>
        `;
    }

    updated(changedProperties){
        console.log("updated en persona-main");

        if (changedProperties.has("showPersonForm")){
            console.log("Ha cambiado valor propiedad showPersonForm en persona-main");

            if (this.showPersonForm === true) {
                this.showPersonFormData();
            } else {
                this.showPersonList();          
            }      
        }

        if (changedProperties.has("people")){
            console.log("Ha cambiado valor propiedad people en persona-main");

            this.dispatchEvent(
                new CustomEvent(
                    "updated-people",
                    {
                        detail : {
                            people : this.people
                        }
                    }
                )
            )
        }
    }

    peopleDataUpdated(e){
        console.log("peopleDataUpdated");
        this.people = e.detail.people;
    }

    personFormClose(){
        console.log("personFormClose");
        console.log("Se ha cerrado el formulario de Persona");

        this.showPersonForm = false;
    }

    personFormStore(e){
        console.log("personFormStore");
        console.log("Se va a almacenar una persona");
        console.log(e.detail.person);

        if (e.detail.editingPerson === true){
            console.log("Se va a actualizar la Persona de  nombre " + e.detail.person.name);    

            //let indexOfPerson = this.people.findIndex(
            //    person => person.name === e.detail.person.name
            //);

            //if (indexOfPerson >= 0) {
            //    console.log("Persona encontrada");
            //    this.people[indexOfPerson] = e.detail.person;
            //}

            this.people = this.people.map(
                person => person.name === e.detail.person.name
                    ? person = e.detail.person : person);
        } else{
            console.log("Se va a almacenar una persona nueva");    
            //this.people.push(e.detail.person);
            //JS SPREAD SYNTAX
            this.people = [...this.people, e.detail.person];
        }

        console.log("Persona almacenada");
        this.showPersonForm = false;
    }

    showPersonFormData(){
        console.log("shorPersonFormData");
        console.log("Mostrando Formulario de Persona");
        this.shadowRoot.getElementById("personForm").classList.remove("d-none");
        this.shadowRoot.getElementById("peopleList").classList.add("d-none");
    }

    showPersonList(){
        console.log("shorPersonList");
        console.log("Mostrando Listado de Personas");
        this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
        this.shadowRoot.getElementById("personForm").classList.add("d-none");
    }

    deletePerson(e){
        console.log("deletePerson en persona-main");
        console.log("Se va a borrar la persona con nombre " + e.detail.name);

        this.people = this.people.filter(
            person => person.name != e.detail.name
        );
    }

    infoPerson(e){
        console.log("infoPerson en persona-main");
        console.log("Se va a mostrar más información de la persona con nombre " + e.detail.name);

        let chosenPerson = this.people.filter(
            person => person.name === e.detail.name
        );

        let person  = {};
        person.name = chosenPerson[0].name;
        person.profile = chosenPerson[0].profile;
        person.yearsInCompany = chosenPerson[0].yearsInCompany;

        this.shadowRoot.getElementById("personForm").person = person;
        this.shadowRoot.getElementById("personForm").editingPerson = true;
        this.showPersonForm = true;

        //console.log(chosenPerson);
        //console.log(chosenPerson[0].name);
    }    
}

customElements.define('persona-main', PersonaMain);