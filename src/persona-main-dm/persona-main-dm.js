import { LitElement, html } from "lit-element";

class PersonaMainDM extends LitElement{

    static get properties(){
        return{
            people:{type: Array}
        };
    }

    constructor(){
        super();

        this.people = [
            {
                name: "Pepe",
                yearsInCompany: 10,
                photo: {
                    src: "./img/persona.jpg",
                    alt: "Photo Pepe"
                },
                profile: "Lorem ipsum dolor sit amet"
            }, {
                name: "Sofia",
                yearsInCompany: 2,
                photo: {
                    src: "./img/persona.jpg",
                    alt: "Photo Sofia"
                },
                profile: "Lorem ipsum."
            },{
                name: "Manel",
                yearsInCompany: 5,
                photo: {
                    src: "./img/persona.jpg",
                    alt: "Photo Manel"
                },
                profile: "Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet"
            },{
                name: "Julia",
                yearsInCompany: 7,
                photo: {
                    src: "./img/persona.jpg",
                    alt: "Photo Julia"
                },
                profile: "Lorem ipsum dolor"
            },{
                name: "Ana",
                yearsInCompany: 11,
                photo: {
                    src: "./img/persona.jpg",
                    alt: "Photo Ana"
                },
                profile: "Lorem ipsum"
            }
        ]        

    }

    updated(changedProperties){
        console.log("updated en persona-main-dm");

        if (changedProperties.has("people")){
            console.log("Ha cambiado valor propiedad people en persona-main-dm");

            this.dispatchEvent(
                new CustomEvent(
                    "persona-data-updated", 
                    {
                        detail : {
                            people: this.people
                        }
                    }
                )
            )        

        }
    }

    render(){
        return html`
            <h5>Persona Main DM</h5>
        `;
    }
}

customElements.define('persona-main-dm', PersonaMainDM);