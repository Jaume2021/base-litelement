import { LitElement, html } from "lit-element";
import '../persona-header/persona-header'
import '../persona-sidebar/persona-sidebar'
import '../persona-main/persona-main'
import '../persona-footer/persona-footer'
import '../persona-stats/persona-stats'

class PersonaApp extends LitElement{

    static get properties(){
        return{
            people:{type: Array}
        };
    }

    constructor(){
        super();
    }
    
    render(){
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
            <persona-header></persona-header>
            <div class="row">
                <persona-sidebar @new-person="${this.newPerson}" class="col-2"></persona-sidebar>
                <persona-main @updated-people="${this.updatedPeople}" class="col-10"></persona-main>
            </div>
            <persona-footer></persona-footer>
            <persona-stats @updated-people-stats="${this.updatedPeopleStats}" ></persona-stats>
        `;
    }

    updated(changedProperties){
        console.log("updated en persona-app");

        if (changedProperties.has("people")){
            console.log("Ha cambiado valor propiedad people en persona-app");

            this.shadowRoot.querySelector("persona-stats").people = this.people;
        }
    }

    updatedPeopleStats(e){
        console.log("updatedPeopleStats");
        console.log(e.detail);
        console.log(e.detail.people);

        this.shadowRoot.querySelector("persona-sidebar").peopleStats = e.detail.peopleStats;
    }

    newPerson(e){
        console.log("newPerson en persona-app");
        this.shadowRoot.querySelector("persona-main").showPersonForm = true;
    }

    updatedPeople(e){
        console.log("updatedPeople en persona-app");
        console.log(e.detail.people);
        this.people = e.detail.people;
    }
}

customElements.define('persona-app', PersonaApp);