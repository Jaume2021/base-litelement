import { LitElement, html } from "lit-element";

class FichaPersona extends LitElement{

    static get properties(){
        return{
            name: {type: String},
            yearsInCompany: {type: Number},
            personInfo: {type: Number}
        }
    }

    constructor(){
        super();
        this.name ="Prueba Nombre";
        this.yearsInCompany = 1;
    }

    updated(changedPropierties){
        console.log("updated");

        changedPropierties.forEach((oldValue, propName) => {
            console.log("Propiedad " + propName + " cambia valor, anterior era " + oldValue)  
        });

        if (changedPropierties.has("name")){
            console.log("propiedad name cambia valor, anterior era "   
                + changedPropierties.get("name") + " nuevo es " + this.name);
        }

        if (changedPropierties.has("yearsInCompany")){
            console.log("propiedad yearsInCompany cambia valor, anterior era "   
                + changedPropierties.get("yearsInCompany") + " nuevo es " + this.yearsInCompany);
            this.updatePersonInfo();
        }
    }   

    render(){
        return html`
            <div>
                <label>Nombre Completo</label>
                <input type="text" id="fname" value="${this.name}" @change="${this.updateName}"></input>
                <br />  
                <label>Años en la Empresa</label>
                <input type="text" value="${this.yearsInCompany}" @change="${this.updateYearsInCompany}"></input>
                <br /> 
                <label>Person Info</label>
                <input type="text" value="${this.personInfo}" disabled></input>
                <br /> 
            </div>
        `;
    }

    updateName(e){
        console.log("UpdateName");
        this.name = e.target.value;
    }

    updateYearsInCompany(e){
        console.log("UpdatYearsInCompany");
        this.yearsInCompany = e.target.value;
    }   
    
    updatePersonInfo(){
        console.log("UpdatePersonInfo");

        if (this.yearsInCompany >= 7){
            this.personInfo ="lead";                
        } else if (this.yearsInCompany >= 5){
            this.personInfo ="senior";      
        } else if (this.yearsInCompany >= 3){
            this.personInfo ="team"; 
        } else {
            this.personInfo ="junior"; 
        }        
    }

}

customElements.define('ficha-persona', FichaPersona);